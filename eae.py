class Bohater:
    imie = ""
    zywotnosc = 0
    punkty_taktyki = 0

    def __init__(self, imie, zywotnosc, punkty_taktyki):
        self.imie = imie
        self.zywotnosc = zywotnosc
        self.punkty_taktyki = punkty_taktyki

    def Zycie(self, wartosc):
        if self.zywotnosc + wartosc < 0:
            self.zywotnosc = 0
        elif self.zywotnosc + wartosc > 100:
            self.zywotnosc = 100
        else:
            self.zywotnosc += wartosc

class Lucznik(Bohater):
    zrecznosc = 0
    moc_ataku = 0
    def __init__(self, zrecznosc, imie, zywotnosc, punkty_taktyki):
        self.imie = imie
        self.zywotnosc = zywotnosc
        self.punkty_taktyki = punkty_taktyki
        self.zrecznosc = zrecznosc

    def print(self):
        print("|Łucznik statystyki|\n Imie: {}\n Żywotność: {}%\n Punkty Taktyki: {}\n Zrecznosc: {}\n Moc ataku: {} ".
              format(self.imie, self.zywotnosc, self.punkty_taktyki, self.zrecznosc,self.moc_ataku))
    def __call__(self):
        self.moc_ataku = self.zrecznosc * self.punkty_taktyki * self.zywotnosc

class Wojownik(Bohater):
    sila = 0
    moc_ataku = 0
    def __init__(self, sila, imie, zywotnosc, punkty_taktyki):
        self.imie = imie
        self.zywotnosc = zywotnosc
        self.punkty_taktyki = punkty_taktyki
        self.sila = sila

    def print(self):
        print("|Wojownik statystyki|\n Imie: {}\n Żywotność: {}%\n Punkty Taktyki: {}\n Siła: {}\n Moc ataku: {}".
              format(self.imie, self.zywotnosc, self.punkty_taktyki, self.sila,self.moc_ataku))
    def __call__(self):
        if self.zywotnosc < 20:
            self.moc_ataku = self.sila * self.punkty_taktyki * 150
            print("\n-------------Wojownik wpada w szał------------\n")
        else:
            self.moc_ataku = self.sila * self.punkty_taktyki * self.zywotnosc

def start():
    klasa = input("Wybierz klasę Postaci 1 - Łucznik   2 - Wojownik: ")
    imie = input("Podaj imie postaci: ")
    zywotnosc = input("Podaj zywotnosc postaci od 0 do 100 (Domyślnie jest 100): ")
    PT = input("Podaj punkty taktyki (domyślnie jest 10): ")

    if int(klasa) == 1:
        zrecznosc = input("Podaj punkty zreczności (Domyślnie jest 15): ")
        if zywotnosc == "":
            zywotnosc = 100
        if PT == "":
            PT = 10
        if zrecznosc == "":
            zrecznosc = 10
        return Lucznik(int(zrecznosc), imie, int(zywotnosc), int(PT))
    elif int(klasa) == 2:
        sila = input("Podaj punkty siły (Domyślnie jest 25): ")
        if zywotnosc == "":
            zywotnosc = 100
        if PT == "":
            PT = 10
        if sila == "":
            sila = 25
        return Wojownik(int(sila), imie, int(zywotnosc), int(PT))


L1 = start()
L1()
t = True
print("1 - wyświetli statystyki postaci \n2 - Zwiększnie / Zmniejszanie żywotnosci \n3 - Zakończ program")

while(t):
    x = input()
    if int(x) == 1:
        L1.print()
    elif int(x) == 2:
        b = input("Dodaj/Odejmij zywotnosc: ")
        L1.Zycie(int(b))
        L1()
    elif int(x) == 3:
        t = False