class Osoba:
    imie = ""
    nazwisko = ""
    rokUrodzenia = 0
    wiek = 0
    def __init__(self,imie,nazwisko,rokUrodzenia):
        self.imie = imie
        self.nazwisko = nazwisko
        self.rokUrodzenia = rokUrodzenia
        self.wiek = 2019 - rokUrodzenia
    def info(self):
        print("| {} | {} | {} | {} |".format(self.imie,self.nazwisko,self.rokUrodzenia,self.wiek))

p1 = Osoba("Kamil","cholipski",1997)
p1.info()
p2 = Osoba("Andrzej","jakiśtam",1947)
p2.info()