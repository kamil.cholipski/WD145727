class Konto:
    def __init__(self, saldo_poczatkowe):
        self.saldo_poczatkowe = saldo_poczatkowe
        self.saldo_koncowe = saldo_poczatkowe

    def wyplata(self, g):
        self.saldo_koncowe -= g

    def spr_stan_slado_koncowe(self):
        print("Obecny stan konta to: {} zł".format(self.saldo_koncowe))

    def spr_stan_slado_poczatkowe(self):
        print("Obecny stan konta to: {} zł".format(self.saldo_poczatkowe))

    def przelew_zewnetrzny(self, x):
        self.saldo_koncowe -= round(float(x) * 1.015, 2)

    def wplata(self, b):
        self.saldo_koncowe += b

    def przelew(self, k2, x):
        self.wyplata(x)
        k2.wplata(x)

kn = Konto(1008)
kn2 = Konto(1078)
kn3 = Konto(541)

kn.spr_stan_slado_poczatkowe()
kn2.spr_stan_slado_poczatkowe()
kn3.spr_stan_slado_poczatkowe()

print("\n ------ \n")
kn.wplata(54)
kn2.wplata(147)

kn.przelew(kn2,78)

kn2.przelew_zewnetrzny(985)

kn.spr_stan_slado_koncowe()
kn2.spr_stan_slado_koncowe()
kn3.spr_stan_slado_koncowe()