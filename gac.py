import numpy as np

a1 = np.array([[6], [7], [-1]])
a2 = np.array([1, 2, 3])
A = a1 * a2
print(A)
b1 = np.array([4, 5, 6])
B = np.matmul(b1, a1)
print(B)
c1 = np.array([[6, 2, 3], [7, -32, 4], [5, 0, -1]])
c2 = np.array([[4, 2, 7], [-2, 5, 7], [9, 0, 4]])
C = c1 @ c2
print(C)
D = a1 + a2
print(D)
E = c1 - c2
print(E)
F = np.linalg.inv(c1)
print(F)
