import pandas as pd

data = {
    "Miasto": ['Olsztyn', 'Warszawa'],
    "Temperatura": ["Prognoza", "Aktualna"],
    "Sty-2015": [2, -5],
    "Lut-2015": [-3, 4],
    "Mar-2015": [5, 12]
}

data2 = pd.DataFrame(data)
data3 = pd.melt(data2, id_vars=["Miasto", "Temperatura"], var_name="Data", value_name="Wartość")