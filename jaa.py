import pandas as pd

data = pd.read_csv('bilety.csv', sep=";", index_col=1,thousands='.', decimal=',')
data.drop(data.columns[0], axis=1, inplace=True)
data.replace(" ","")
data2 = data.stack()

srednia  = data2.mean()
minGlobal = data2.min()
maxGlobal = data2.max()
maxWoj = data.max(axis=1)
minWoj = data.min(axis=1)
lp = data2[data2 == minGlobal]
lp2 = data2[data2 == maxGlobal]
sredWoj = data.mean(axis=1)

WzrostCenyProcentowo = (data.iloc[:,len(data.columns) - 1] * 100 ) / data.iloc[:,0] - 100

