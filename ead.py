class Konto:
    def __init__(self, saldo_poczatkowe):
        self.saldo_poczatkowe = saldo_poczatkowe
        self.saldo_koncowe = saldo_poczatkowe

    def wyplata(self, g):
        self.saldo_koncowe -= g

    def spr_stan_slado_koncowe(self):
        print("Obecny stan konta to: {} zł".format(self.saldo_koncowe))

    def spr_stan_slado_poczatkowe(self):
        print("Obecny stan konta to: {} zł".format(self.saldo_poczatkowe))

    def przelew_zewnetrzny(self, x):
        self.saldo_koncowe -= round(float(x) * 1.015, 2)

    def wplata(self, b):
        self.saldo_koncowe += b

    def przelew(self, k2, x):
        self.wyplata(x)
        k2.wplata(x)


class KontoPrywatne(Konto):
    def PrzelewWynagrodzenia(self, pensja=1000):
        self.saldo_koncowe += pensja

    def debet(self, deb=500):
        self.saldo_koncowe += deb


class KontoFirmowe(Konto):
    def PrzelewZUS(self, x):
        self.saldo_koncowe -= x
        ZUS.saldo_koncowe += x

    def PrzelewUS(self, x):
        self.saldo_koncowe -= x
        US.saldo_koncowe += x


ZUS = Konto(0)
US = Konto(0)

konto1 = KontoPrywatne(1754)
konto2 = KontoPrywatne(105)
konto3 = KontoFirmowe(38547)

konto1.spr_stan_slado_poczatkowe()
konto2.spr_stan_slado_poczatkowe()
konto3.spr_stan_slado_poczatkowe()

konto1.PrzelewWynagrodzenia()
konto2.debet()

konto3.PrzelewZUS(3547)
konto3.PrzelewUS(8547)
print("\n----------------\n")
konto1.spr_stan_slado_koncowe()
konto2.spr_stan_slado_koncowe()
konto3.spr_stan_slado_koncowe()
