import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
df = pd.read_csv("cars.csv", sep=";")
df.drop([0], inplace=True)
df2 = df.groupby(['Origin']).count()
bars = df2.index.values.tolist()
height = df2.iloc[0:,1]
y_pos = np.arange(len(bars))
plt.bar(y_pos, height, color=['black', 'red', 'green'])
plt.xticks(y_pos, bars)
plt.ylabel('Ilość Modeli')
plt.show()
