import pandas as pd

Cars = {'Marka': ['Honda Civic', 'Toyota Corolla', 'Ford Focus', 'Audi A4'],
        'Cena': [22000, 25000, 27000, 35000],
        'Rok': [2015, 2013, 2018, 2018]
        }

df = pd.DataFrame(Cars, columns=['Marka', 'Cena', 'Rok'])

df.sort_values(by=['Rok'],ascending=False,  inplace=True)

df["test"] =  df["Cena"] * 1.23

df = df.rename(columns={'test':'Cena Brutto'})

paliwo = pd.Series(['Benzyna','Diesel','Benzyna'])
df['Rodzaj_Paliwa'] = paliwo

df.drop(columns=['Cena'],inplace=True)

s = df.size

df1 = pd.DataFrame([(1,2),(3,4),(3,4),(5,6)], columns=['a','b'])
df2 = pd.DataFrame([(100,200),(300,400),(300,450),(500,600)], columns=['a','b'])
df_add = df1.add(df2, fill_value=0)

df_add.sort_values(by=['a','b'],ascending=True,  inplace=True)
df1_add = df_add.transpose()
