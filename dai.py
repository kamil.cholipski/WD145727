def zliczkw(*zmienne):
    po = [n**3 for n in zmienne]
    return po

print(zliczkw(1, 2, 5))
print(zliczkw(1, 2, 5, 8, 9, 6, 2, 1, 4, 11))
print(zliczkw(1, 2, 5, 7, 8, 9))