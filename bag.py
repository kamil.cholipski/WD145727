x = 5
z2 = 3.141592653589793
z3 = 11
z4 = 'balony'
z5 = 'czerwone'
z6 = 'białe'
data = {'imie': 'kamil', 'nazwisko': 'cholipski'}
print('IMIĘ: {imie} \nNAZWISKO: {nazwisko}'.format(**data))
print('{0:2d} {1:3d} {2:4d}'.format(x, x * x, x * x * x))
print('{:06.2f}'.format(z2))
print('Ta osoba ma {} i {} {}'.format(z6, z5, z4))
print("{:*^20s}".format(z4))
print("{0} binarnie to {0:b}".format(z3))
