import pandas as pd


data = pd.read_csv('cars.csv', sep=";")
data.drop([0], inplace=True)
data['Horsepower'] = data['Horsepower'].astype(float)
data['MPG'] = data['MPG'].astype(float)
data['Cylinders'] = data['Cylinders'].astype(float)
data['Acceleration'] = data['Acceleration'].astype(float)

data2 = data.groupby(['Origin']).count()
data2 = data2.iloc[0:,1]

maxHp = data['Horsepower'].max()
data3 = data[data["Horsepower"] == maxHp]

data5 = data[data['MPG'] != 0]
NajnizszeSpalanie = data5["MPG"].max()
data4 = data[data['MPG'] == NajnizszeSpalanie]
NajwyzszeSpalanie = data5['MPG'].min()

PojazdySpalanie = data[(data['MPG'] == NajnizszeSpalanie) | (data['MPG'] == NajwyzszeSpalanie)]

srednie = data5['MPG'].mean()
data6 = data[data['Origin'] == 'Europe']
SredniaHP_Europa = data6['Horsepower'].mean()