class Zespolone():
    def __init__(self, liczba):
        self.liczba = liczba
    def czesc(self):
        print("Część Urojona: ", self.liczba.imag)
        print("Część rzeczywista: ", self.liczba.real)
    def potega(self,n):
        print("liczba zespolona do potegi {} to : {}".format(n,self.liczba ** n))
    def sprzezenie(self):
        print("Sprzężenie: ", self.liczba.conjugate())
    def modul(self):
        print("Moduł: ", abs(self.liczba))

c1 = Zespolone(2 + 5j)

c1.czesc()
c1.potega(2)
c1.sprzezenie()
c1. modul()
