| cw 1| cw 2 | cw 3| cw 4| cw5| Cw 6| kolo 1 | cw 8 | cw 9 | cw 10 | cw 11| cw 12| kolo2|
|---|---|---|---|---|---|---|---|---|---|---|---|---|
| 2 pkt | 2 pkt | 2 pkt| 2 pkt| 2 pkt| 2 pkt| 50 pkt | 2 pkt | 2 pkt | 2 pkt | 2 pkt| 2 pkt| 39 pkt|

**Ocena na koniec z cw i egzaminu: 5,0 (bdb). Gratulacje!**

Uwagi:

### cw

* aae - w mianowniku `2*a` powinno być w nawiasie

### cw2

* baa - w linijce 25 jest błąd

### cw 4

* wszystko ok

### cw 6

* faa - nie jest to przedmiot związany z algorytmami, więc na kolokwium zaliczę zadanie, ale warto na przyszłość pomyśleć jak lepiej napisać algorytm lepiej

### cw 8

* gac - lepiej jednowymiarowe macierze "opakować" w dwuwymiarową

### cw 9

* ok

### cw 10

ok
