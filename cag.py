def spr_pesel(pesel):
    mnozniki_sumy = [9, 7, 3, 1, 9, 7, 3, 1, 9, 7]
    suma_kontrolna = 0
    i = 0
    while i < 10:
        suma_kontrolna += (int(pesel[i]) * mnozniki_sumy[i])
        i += 1
    if (suma_kontrolna % 10) != int(pesel[10]):
        return False
    else:
        return True


def wyswietlanie_danych(pesel):
    miesiace = {
        1: "Styczeń", 2: "Lusty", 3: "Marzec", 4: "Kwiecień", 5: "Maj", 6: "Czerwiec", 7: "Lipiec", 8: "Sierpień",
        9: "Wrzesień", 10: "Październik", 11: "Listopad", 12: "Grudzień"
    }
    rok = int(pesel[0:2])
    msc = int(pesel[2:4])
    dzien = int(pesel[4:6])
    if 81 <= msc <= 92:
        rok += 1800
        msc -= 80
    elif 1 <= msc <= 12:
        rok += 1900
    elif 21 <= msc <= 32:
        rok += 2000
        msc -= 20
    elif 41 <= msc <= 52:
        rok += 2100
        msc -= 40
    elif 61 <= msc <= 72:
        rok += 2200
        msc -= 60

    print("Data urodzenia: {} {} {}".format(dzien, miesiace[msc], rok))
    if int(pesel[9]) % 2 == 0:
        print("Płeć: Kobieta")
    else:
        print("Płeć: Mężczyzna")


x = input("Podaj numer pesel: ")

if len(x) != 11:
    print("Długość numeru pesel jest niepoprawna")
    exit()
elif not x.isnumeric():
    print("W numerze pesel występują znaki niebędące cyfrą")
    exit()
elif not spr_pesel(x):
    print("Numer pesel jest niepoprawny - Suma kontrolna jest błedna")
    exit()

wyswietlanie_danych(x)
