def silnia(n):
    if n < 2:
        return 1
    else:
        return silnia(n - 1) * n


print("5! = ", silnia(5))
print("0! = ", silnia(0))
print("1! = ", silnia(1))
print("2! = ", silnia(2))
