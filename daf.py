def czyposortowane(a):
    b = a.copy()
    b.sort(reverse=True)
    if a == b:
        return True
    else:
        return False


lista = [9, 5, 4, 3, 3, 2, 1]
print("Czy lista {} jest posortowana malejąco? {}".format(lista,czyposortowane(lista)))
