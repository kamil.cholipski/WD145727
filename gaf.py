import numpy as np

woj = np.array(
    ["Dolnośląskie", "Kujawsko-pomorskie", "Lubelskie", "Lubuskie", "Łódzkie", "Małopolskie", "Mazowieckie", "Opolskie",
     "Podkarpackie", "Podlaskie", "Pomorskie", "Śląskie", "Świetokrzyskie", "Warminsko-mazurskie", "Wielkopolskie",
     "Zachodniopomorskie"])
pow = np.array([1994670, 1797134, 2512246, 1398793, 1821895, 1518279, 3555847, 941187, 1784576, 2018702, 1831034,
                1233309, 1171050, 2417347, 2982650, 2289248])
lud = np.array([19947, 17972, 25122, 13988, 18219, 15183, 35558, 9412, 17846, 20187, 18310,  12333, 11711, 24173,
                29826, 22892])

A = lud * 100/pow
print(A)
print("\n")
b1 = -np.sort(-pow)
b1 = b1[0:3]
b2 = np.in1d(pow,b1)
B = woj[b2]
print(B)
print("\n")
C = woj[(lud * 100) < 1000000]
print(C)