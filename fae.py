def rek(m):
    if m == 0:
        return 1
    elif m == 1:
        return 1
    else:
        return 4*rek(m-1)+5

print(rek(5))
print(rek(0))
print(rek(1))
print(rek(2))
print(rek(4))