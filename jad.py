import matplotlib.pyplot as plt
import numpy as np
x = np.linspace(-5, 5, 15)
plt.plot(x, np.e**x, 'r')
plt.grid(True)
plt.xlim(-5, 5)
plt.savefig("fig3.png", dpi=72)
plt.show()