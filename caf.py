A = set(["Kalisz", "Malbork", "Olsztyn", "Poznań", "Warszawa", "Gdańsk"])
B = set(["Kalisz", "Malbork", "Olsztyn", "Poznań", "Warszawa", "Łódź", "Łomża", "Kraków"])
C = set(["Kalisz", "Malbork", "Olsztyn", "Opole", "Gdynia", "Ostróda"])

print("A Ս B Ս C = ", A.union(B, C))
print("A Ո B Ո C = ", A.intersection(B, C))
print("A \ B = ", A.difference(B))
