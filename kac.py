import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
df = pd.read_csv('kolej.csv',sep=';')
df2 = df.sort_values('linie kolejowe eksploatowane;2017;[km]',axis=0,ascending=False)
labels2 = df2['Nazwa']
labels2 = labels2.iloc[1:]
df3 = df2.iloc[1:,[2]]
explode = ([0.1 for x in range(3)] + [0 for x in range(13)])
fig1, ax1 = plt.subplots()
ax1.pie(df3, explode=explode, labels=labels2, autopct='%1.1f%%',
shadow=True, startangle=90)
ax1.axis('equal')
plt.show()