def cg(n, a1=1, q=2):
    an = q ** (n - 1) * a1
    return an


p1 = [3, 3, 5]
p2 = [3, 0, 1]
p3 = [4, 7, 4]

print("n = {} a1 = {} q = {} an = {}".
      format(p1[0], p1[1], p1[2], cg(p1[0], p1[1], p1[2])))
print("n = {} a1 = {} q = {} an = {}".
      format(p2[0], p2[1], p2[2], cg(p2[0], p2[1], p2[2])))
print("n = {} a1 = {} q = {} an = {}".
      format(p3[0], p3[1], p3[2], cg(p3[0], p3[1], p3[2])))

print("n = 6 a1 = 1 q = 2 an = ", cg(6))
