def srednia(**kwargs):
    sum = 0
    i = 0
    for key, value in kwargs.items():
        sum += value
        i += 1
    return sum / i


print(srednia(Adam=52, Michał=24, Kamil=22, Maciej=31))
print(srednia(Adam=44, Michał=12, Kamil=33, Maciej=81, Robert=55))
print(srednia(Adam=12, Michał=44, Kamil=35, Maciej=25, Marcin=24))
