liczby_rzymskie = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}


def wynik(x):
    i = 0
    suma = 0
    z = int(len(x))
    if z == 1:
        return liczby_rzymskie[x]
    else:
        while i != z:
            if i + 1 >= z:
                suma = suma + liczby_rzymskie[x[i]]
            else:
                if (x[i] == "I" and x[i + 1] == "X") \
                        or (x[i] == "I" and x[i + 1] == "V") \
                        or (x[i] == "X" and x[i + 1] == "C") \
                        or (x[i] == "X" and x[i + 1] == "L") \
                        or (x[i] == "C" and x[i + 1] == "D") \
                        or (x[i] == "C" and x[i + 1] == "M"):
                    suma = suma + liczby_rzymskie[x[i + 1]] - liczby_rzymskie[x[i]]
                    i = i + 1
                else:
                    suma = suma + liczby_rzymskie[x[i]]

            i = i + 1
        return suma

def zlicz_znaki(x):
    z = int(len(x))
    ilosc_znak = 1
    i = 0
    while i != z -1:
        if x[i] == x[i + 1]:
            ilosc_znak += 1
        i+=1
    return ilosc_znak

def liczba_rzymska(liczba):
    x = 0
    z = int(len(liczba))
    if z == 1:
        return wynik(liczba)
    else:
        while x != z - 1:
            if (liczba[x] == "V" and liczba[x + 1] == "V") \
                    or (liczba[x] == "L" and liczba[x + 1] == "L") \
                    or (liczba[x] == "D" and liczba[x + 1] == "D"):
                print("Obok siebie nie mogą stać dwa znaki: V, L, D.")
                exit()
            if zlicz_znaki(liczba) > 3:
                print("Obok siebie mogą stać co najwyżej trzy znaki spośród: I, X, C lub M.")
                exit()
            if liczby_rzymskie[liczba[x]] < liczby_rzymskie[liczba[x + 1]]:
                if (liczba[x] == "I" and (liczba[x + 1] == "V" or liczba[x + 1] == "X")) \
                        or (liczba[x] == "X" and (liczba[x + 1] == "L" or liczba[x + 1] == "C")) \
                        or (liczba[x] == "C" and (liczba[x + 1] == "D" or liczba[x + 1] == "M")):
                    return wynik(liczba)
                else:
                    print("Błąd, znakami porzedzającymi znak oznaczający większą liczbę mogą być tylko znaki: I, X, C.")
                    exit()
            return wynik(liczba)
            x += 1


x = input("Podaj liczbe rzymska (I, V, X, L, C, D, M): ")
x = x.upper()

print("Liczba Rzymska '{}' to: {}".format(x,liczba_rzymska(x)))
