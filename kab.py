import numpy as np
import matplotlib.pyplot as plt
fig, ax1 = plt.subplots()
t = np.arange(0.1, 5.0, 0.07)
s1 = 1/t
ax1.plot(t, s1, 'b-')
ax1.set_xlabel('time (s)')
ax1.set_ylabel('exp', color='b')
ax1.tick_params('y', colors='b')
ax2 = ax1.twinx()
s2 = 100 + np.sin(t)
ax2.plot(t, s2, 'r.')
ax2.set_ylabel('100 + sin', color='r')
ax2.tick_params('y', colors='r')
ax1.set_ylim(0,200)
fig.tight_layout()
plt.show()