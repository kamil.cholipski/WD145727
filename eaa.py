class Car:
    def __init__(self, marka, rok):
        self.marka = marka
        self.rok = rok


car1 = Car("BMW", 2001)
print(car1.marka, car1.rok)
car2 = Car("VW", 2007)
print(car2.marka, car2.rok)
car1 = car2
print(car1.marka, car1.rok)