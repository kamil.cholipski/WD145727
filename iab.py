import pandas as pd
import numpy as np

studenci = [ ['jack', 34, 'Sydeny'] ,
             ['Riti', 30, 'Delhi' ] ,
             ['Aadi', 16, 'New York'] ]

dlista = pd.DataFrame(studenci)

data = {'kolumna_1': [3, 2, 1, 0], 'kolumna_2': ['a', 'b', 'c', 'd']}
b = pd.DataFrame.from_dict(data)

data= np.array([[ 5.8,2.8], [ 6.0,2.2]])
data2 = pd.DataFrame({'Kolumna1':data[:,0],'Kolumna2':data[:,1]})

sr = pd.Series([19.5, 16.8, 22.78, 20.124, 18.1002])
sr2 = sr.to_frame()

Produkty = {'Produkt': ['Tablet','iPhone','Laptop','Monitor'],
            'Cena': [250,800,1200,300]
            }

df = pd.DataFrame(Produkty, columns= ['Produkt', 'Cena'])

Produkty_lista = df.values.tolist()
Produkty_slownik = df.to_dict()
Produkty_tab = df.values
Produkty_serie = pd.Series(df['Produkt'])

type(Produkty_lista)
type(Produkty_slownik)
type(Produkty_tab)
type(Produkty_serie)
