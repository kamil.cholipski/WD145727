import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-7,7,100)

plt.plot(x, np.e**x, 'blue', linestyle="-", label="niebieski")
plt.plot(x, x**3, 'red', linestyle=":", label="czerwony")
plt.plot(x, x**4, 'green', linestyle="--", label="zielony")
plt.legend(title='Legenda:')
plt.show()
