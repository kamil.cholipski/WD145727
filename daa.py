def suma(a, b):
    p = a + b
    return p


a1 = 5
a2 = 3
b1 = -4
b2 = 2
c1 = 5.23
c2 = 9

print("{} + {} = {}".format(a1, a2, suma(a1, a2)))
print("{} + {} = {}".format(b1, b2, suma(-b1, b2)))
print("{} + {} = {}".format(c1, c2, suma(c1, c2)))
