liczby = { 0: "zero", 1: "jeden", 2: "dwa", 3: "trzy", 4: "cztery", 5: "pięć", 6: "sześć", 7: "siedem", 8: "osiem", 9: "dziewięć"}

x = input("Podaj liczbe: ")
for i in range(int(len(x))):
    if not x[i].isnumeric():
        continue
    print(liczby[int(x[i])], end=" ")
