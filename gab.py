import numpy as np


def Zastap(A, x):
    return np.where(A == 0, x, A)


A = np.zeros((2, 6), dtype=int)
print(Zastap(A, 5))
print(A)
