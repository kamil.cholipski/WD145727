import numpy as np

imiona = np.array(["Anna", "Zofia", "Sylwia", "Katarzyna", "Teresa", "Tomasz", "Cezary", "Zenon", "Filip", "Adrian"])
wiek = np.array([21, 40, 13, 31, 34, 14, 13, 28, 20, 15])
plec = np.array(["K", "K", "K", "K", "K", "M", "M", "M", "M", "M"]);
waga = np.array([65, 80, 64, 69, 74, 61, 66, 61, 69, 77])
wzrost = np.array([179, 179, 151, 177, 170, 157, 151, 153, 160, 160])
okulary = np.array([False, True, False, True, False, True, False, True, False, True])

A = np.sort(imiona)
print(A)
B = imiona[okulary == True]
print(B)
c1 = np.logical_and(wiek >= 20, wiek <= 30)
c2 = np.logical_and(c1, plec == "K")
C = imiona[c2]
print(C)
d1 = np.logical_and(waga >= 60, waga <= 80)
d2 = np.logical_and(wzrost >= 160, wzrost <= 180)
d3 = np.logical_and(d1, d2)
d4 = np.logical_and(d3, okulary == False)
D = imiona[d4]
print(D)
BMI = waga / wzrost ** 2
print(BMI)
F = np.average(wiek)
idx = (np.abs(wiek - F)).argmin()
print(imiona[idx])
