import numpy as np

x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print(np.shape(x))
x.shape = (2, 5)
print(np.shape(x))
print(x)
x += 2
print(x)
