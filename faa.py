klucz = {"a": "y", "e": "i", "i": "o", "o": "a", "y": "e"}

def szyfr(m):
    list = []
    for x in range(len(m)):
        if m[x] not in klucz:
            #print(m[x],end="")
            list.append(m[x])
        else:
            #print(klucz[m[x]], end="")
            list.append(klucz[m[x]])
    return list


def rozszyfruj(m):
    keylist = list(klucz.keys())
    valist = list(klucz.values())
    lista = []
    for x in range(len(m)):
        if m[x] not in klucz.values():
            #print(m[x],end="")
            lista.append(m[x])
        else:
            #print(keylist[valist.index(m[x])],end="")
            lista.append(keylist[valist.index(m[x])])
    return lista


x = input("p: ")
print(*szyfr(x),sep="")
print("\n")
print(*rozszyfruj((szyfr(x))),sep="")