def silnia(n):
    iloczyn = 1
    i = 1
    while i <= n:
        iloczyn *= i
        i += 1
    return iloczyn


print("5! = ", silnia(5))
print("0! = ", silnia(0))
print("1! = ", silnia(1))
print("2! = ", silnia(2))
