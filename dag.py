def zliczkw(*zmienne):
    suma = 0
    dl = len(zmienne)
    for x in range(dl):
        suma += zmienne[x] ** 2
    return suma


print(zliczkw(1, 2, 5))
print(zliczkw(1, 2, 5, 8, 9, 6, 2, 1, 4, 11))
print(zliczkw(1, 2, 5, 7, 8, 9))
